package java01;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.text.StyleContext.SmallAttributeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
public class mark {
	public static void main(String[] args) throws IOException{
		double ketang = 0,biancheng=0,fujia=0,test=0,keqian=0;
		//按照阿荣的要求计算云班成绩，就需要读取云班大小班的数据
		File smallfile= new File("src/small.html" );
		File allfile= new File("src/all.html" ) ;
		
		//利用Jsoup爬虫
		Document small =  Jsoup.parse(smallfile, "UTF-8");
		Document all =  Jsoup.parse(allfile, "UTF-8");
		
		//读取配置文件
		Properties p = new Properties();
		p.load(new FileInputStream(".\\src\\total.properties"));
		
		//根据class名选择需要的部分
		int before = Integer.parseInt(p.getProperty("before"));
		int base = Integer.parseInt(p.getProperty("base"));
		int tests = Integer.parseInt(p.getProperty("test"));
		int program = Integer.parseInt(p.getProperty("program"));
		int add = Integer.parseInt(p.getProperty("add"));
		
		Elements smallel =  small.getElementsByClass("interaction-row");
		Elements allel =  all.getElementsByClass("interaction-row");
		
		for (int j = 0; j < allel.size(); j++) {
			
			Element allz = allel.get(j).child(1).child(2).child(0);
			String text = allz.text();
			boolean kt = allel.get(j).text().contains("课前自测");
		    if (kt  ) {
				String temp = (allz.child(10).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				keqian+=t;
				
			}
		    
		}
//		课堂自测部分的总分
		for (int j = 1; j < smallel.size(); j++) {
			
			Element smallz = smallel.get(j).child(1).child(2).child(0);
			String text = smallz.text();
		    boolean kt = smallel.get(j).text().contains("课前自测");
		    if (kt  ) {
				String temp = (smallz.child(10).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				keqian+=t;
				
			}
		    
		}
		System.out.println("课前自测得分="+keqian);
//		课堂完成部分的总分
		for (int j = 1; j < smallel.size(); j++) {
			
			Element smallz = smallel.get(j).child(1).child(2).child(0);
			String text = smallz.text();
		    boolean cy = text.contains("已参与");
		    boolean kt = smallel.get(j).text().contains("课堂完成");
		    if (cy && kt  ) {
				String temp = (smallz.child(7).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				ketang+=t;
				
			}
		    
		}
		System.out.println("课堂完成得分="+ketang);

		
//		课堂小测完成部分的总分
		for (int j = 1; j < smallel.size(); j++) {
			
			Element smallz = smallel.get(j).child(1).child(2).child(0);
			String text = smallz.text();
		    boolean cy = text.contains("已参与");
		    boolean kt = smallel.get(j).text().contains("课堂小测");
		    if (cy && kt  ) {
				String temp = (smallz.child(7).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				test+=t;
			}   
		}
		System.out.println("课堂小测得分="+test);
//		编程题部分的总分
		for (int j = 1; j < smallel.size(); j++) {
			Element smallz = smallel.get(j).child(1).child(2).child(0);
			String text = smallz.text();
		    boolean cy = text.contains("已参与");
		    boolean kt = smallel.get(j).text().contains("编程题");
		    if (cy && kt  ) {
				String temp = (smallz.child(7).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				biancheng+=t;
				
			}
		    
		}
		System.out.println("编程题得分="+biancheng);
//		附加题部分的总分
		for (int j = 1; j < smallel.size(); j++) {
			Element smallz = smallel.get(j).child(1).child(2).child(0);
			String text = smallz.text();
		    boolean cy = text.contains("已参与");
		    boolean kt = smallel.get(j).text().contains("附加题");
		    if (cy && kt  ) {
				String temp = (smallz.child(7).text().replaceAll("经验","").replaceAll(" ",""));
				int t =Integer.parseInt(temp);
				fujia+=t;
				
			}
		    
		}
		System.out.println("附加题得分="+fujia);

		
		double finalBefore = keqian * 0.25 / before * 100 ;
		double finalBase = ketang * 0.3/ base * 100 * 0.95;
		double finalTest = test * 0.2/ tests * 100;
		program = 95;
		double finalProgram = biancheng * 0.1/ program * 100;
		add = 90;
		double finalAdd = fujia * 0.05/ add * 100;
		double finalScore = (finalBefore + finalBase + finalTest + finalProgram + finalAdd) * 0.9 + 6;
//		总分
		System.out.println(String.format("总分=%.5s", finalScore));

		
	}

}
